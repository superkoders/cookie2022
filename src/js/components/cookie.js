import { delegate, on as onEvent } from '@superkoders/sk-tools/src/event';
import { addCookie, removeCookie } from '@superkoders/sk-tools/src/cookie';
import { v4 as uuidv4 } from 'uuid';
import { query } from '@superkoders/sk-tools/src/selector';
import { create } from '@superkoders/sk-tools/src/emmiter';

const acceptPredicate = () => true;
const rejectPredicate = (name) => ['functionality_storage', 'security_storage'].includes(name);
const optionPredicate = (name) => {
	return (
		rejectPredicate(name) ||
		query(`input[data-cookie-option="${name}"]`)?.[0]?.checked ||
		(['ad_personalization', 'ad_user_data'].includes(name) && query(`input[data-cookie-option="ad_storage"]`)?.[0]?.checked)
	);
};
const storageReducer = (predicate = acceptPredicate) => (acc, name) => ({
	...acc,
	[name]: predicate(name) ? 'granted' : 'denied',
});

const emmiter = create({});
const storages = [
	'functionality_storage',
	'security_storage',
	'ad_storage',
	'analytics_storage',
	'personalization_storage',
	'ad_personalization',
	'ad_user_data',
];

let cookieState = window.cookieState || {
	id: `${uuidv4()}${new Date().getTime()}`,
	datetime: new Date().getTime(),
	storages: storages.reduce(storageReducer(rejectPredicate), {}),
};

export const on = (type, fn) => {
	emmiter.on(type, fn);
};
export const one = (type, fn) => {
	emmiter.one(type, fn);
};
export const off = (type, fn) => {
	emmiter.off(type, fn);
};

export const getCurrentState = () => {
	return { ...cookieState };
};

export const deleteConsent = () => {
	removeCookie('SKcookieConsent');
	emmiter.trigger('delete');
};
export const didUserConsent = () => {
	return cookieState.datetime != null;
};

export const allowedCategory = (storage) => {
	return cookieState.storages[storage] === 'granted';
};

export const checkEmbeds = () => {
	query('iframe[data-src][data-cookieconsent]').forEach((node) => {
		if (!node.dataset.cookieconsent.split(/,\s+/).every(allowedCategory)) return;
		delete node.dataset.cookieconsent;
		node.src = node.dataset.src;
	});
};
export const checkScripts = () => {
	query('script[type="text/plain"][data-cookieconsent]').forEach((node) => {
		if (!node.dataset.cookieconsent.split(/,\s+/).every(allowedCategory)) return;
		delete node.dataset.cookieconsent;
		const script = node.cloneNode(true);
		script.type = 'text/javascript';
		node.parentNode.replaceChild(script, node);
	});
};

export const open = () => {
	document.documentElement.dataset.showCookie = true;
	query(`input[data-cookie-option]`).forEach((input) => {
		input.checked = allowedCategory(input.dataset.cookieOption);
	});
	emmiter.trigger('open', {
		...cookieState,
	});
};

export const settings = () => {
	document.documentElement.dataset.cookieState = 'settings';
	emmiter.trigger('settings', {
		...cookieState,
	});
};

export const onUpdate = (event, data) => {
	const dataLayerName = options.dataLayerName;
	window[dataLayerName] = window[dataLayerName] || [];

	function gtag() {
		window[dataLayerName].push(arguments);
	}

	gtag('consent', 'update', {
		...data.storages,
	});

	gtag('set', 'url_passthrough', data.storages['ad_storage'] === 'denied' || data.storages['analytics_storage'] === 'denied');
	gtag('set', 'ads_data_redaction', data.storages['ad_storage'] === 'denied');

	window[dataLayerName].push({ event: options.eventUpdateName });
};

let options = {
	storeURL: null,
	cookieName: 'SKcookieConsent',
	dataLayerName: 'dataLayer',
	eventUpdateName: 'consent-update',
	onUpdate,
};

const updateCookie = (newStorages) => {
	cookieState = { ...cookieState, datetime: new Date().getTime(), storages: newStorages };
	const isAllGranted = storages.every((key) => newStorages[key] === 'granted');
	const expiration = isAllGranted ? 365 : 30;
	addCookie(options.cookieName, JSON.stringify(cookieState), expiration);
	document.documentElement.dataset.showCookie = false;
	window.cookieState = cookieState;

	if (options.storeURL && navigator && navigator.sendBeacon) {
		navigator.sendBeacon(options.storeURL, JSON.stringify(cookieState));
	}

	emmiter.trigger('update', {
		...cookieState,
	});
};

const actions = {
	open: ({ event }) => {
		event.preventDefault();
		open();
	},
	settings: ({ event }) => {
		event.preventDefault();
		settings();
	},
	save: ({ event }) => {
		event.preventDefault();
		updateCookie(storages.reduce(storageReducer(optionPredicate), {}));
	},
	accept: ({ event }) => {
		event.preventDefault();
		updateCookie(storages.reduce(storageReducer(acceptPredicate), {}));
	},
	reject: ({ event }) => {
		event.preventDefault();
		updateCookie(storages.reduce(storageReducer(rejectPredicate), {}));
	},
	toggle: ({ event, button }) => {
		if (event.target.closest('label, a')) return;
		event.preventDefault();
		const state = button.dataset.cookieToggle === 'true';
		button.dataset.cookieToggle = !state;
	},
};

export const init = (initOptions) => {
	options = { ...options, ...initOptions };

	if (typeof options.onUpdate === 'function') {
		on('update', options.onUpdate);
	}

	Object.keys(actions).forEach((key) => {
		onEvent(
			document,
			'click',
			delegate(`[data-cookie-${key}]`, (event, button) => {
				actions[key]({ event, button });
			}),
		);
	});
};
