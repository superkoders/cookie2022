import * as cookie from './components/cookie.js';

window.App = {
	run() {
		cookie.init();
		cookie.checkEmbeds();
		cookie.checkScripts();
		cookie.on('update', cookie.checkEmbeds);
		cookie.on('update', cookie.checkScripts);

		// DEMO for Calibra custom events
		// window.dataLayer = window.dataLayer || [];
		// const storages = ['ad_storage', 'analytics_storage'];

		// cookie.on('update', (event, data) => {
		// 	const isOptIn = storages.every((key) => data.storages[key] === 'granted');
		// 	const isOptOut = storages.every((key) => data.storages[key] !== 'granted');

		// 	window.dataLayer.push({
		// 		event: isOptOut
		// 			? 'consentcookie_banner_optout'
		// 			: isOptIn
		// 			? 'consentcookie_banner_optin_all'
		// 			: data.storages['ad_storage'] === 'granted'
		// 			? 'consentcookie_banner_optin_marketing'
		// 			: 'consentcookie_banner_optin_statistics',
		// 		consentID: data.id,
		// 	});

		// 	window.dataLayer.push({ event: 'consent-update' });
		// });

		// cookie.on('open', (event, data) => {
		// 	window.dataLayer.push({
		// 		event: 'consentcookie_banner_open',
		// 		consentID: data.id,
		// 	});
		// });
	},
};
