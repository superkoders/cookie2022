# [2.4.0](https://bitbucket.org/superkoders/cookie2022/compare/v2.3.1...v2.4.0) (2024-01-17)


### Bug Fixes

* changelog ([18aef7f](https://bitbucket.org/superkoders/cookie2022/commits/18aef7fa92a574939065da128b08c44196d2ced1))
* sk tools dependency ([1aaab0e](https://bitbucket.org/superkoders/cookie2022/commits/1aaab0e24fea11bb9f104627f389ef698bf887a0))
* skip consent by url parameter ([c9a84d4](https://bitbucket.org/superkoders/cookie2022/commits/c9a84d4288b6315db9984ee5ca692d2f4740542d))


### Features

* upgrade to consent mode v2 init ([74c8bf4](https://bitbucket.org/superkoders/cookie2022/commits/74c8bf4cd55ed42f5753db9e4773da43d205a8d5))
* upgrade to consent mode v2 JS ([66284ac](https://bitbucket.org/superkoders/cookie2022/commits/66284ac093f97141df78d1cc462063154d37b62b))



## [2.3.1](https://bitbucket.org/superkoders/cookie2022/compare/v2.3.0...v2.3.1) (2022-03-16)


### Bug Fixes

* gtag storages not work ([2a63bef](https://bitbucket.org/superkoders/cookie2022/commits/2a63befd162727acd5675e4499978981c26a66bf))



# [2.3.0](https://bitbucket.org/superkoders/cookie2022/compare/v2.2.0...v2.3.0) (2022-03-11)


### Features

* dataLayers can be changed now by cookie options ([627b420](https://bitbucket.org/superkoders/cookie2022/commits/627b420ae13df2620aea37b37ca9bca3df01487a))



# [2.2.0](https://bitbucket.org/superkoders/cookie2022/compare/v2.1.3...v2.2.0) (2022-02-28)


### Bug Fixes

* Calibra demo ([59202f9](https://bitbucket.org/superkoders/cookie2022/commits/59202f97bf93ab6da55e6a657e0967c0e0a1aeb6))
* incomplete consent expiry in 30 days ([c6903ed](https://bitbucket.org/superkoders/cookie2022/commits/c6903edfccc97923a91f5b62d314969dfa8d7c89))


### Features

* emit more events ([e93bd48](https://bitbucket.org/superkoders/cookie2022/commits/e93bd481c254affacaaa00f31f517ff5df69a7bc))



## [2.1.3](https://bitbucket.org/superkoders/cookie2022/compare/v2.1.2...v2.1.3) (2022-01-28)


### Features

* add open method ([0c5f880](https://bitbucket.org/superkoders/cookie2022/commits/0c5f88036492b921cb8287d3516f10aa7c5ccc76))



## [2.1.2](https://bitbucket.org/superkoders/cookie2022/compare/v2.1.0...v2.1.2) (2022-01-28)


### Bug Fixes

* remove typo from compiled css ([451ccfc](https://bitbucket.org/superkoders/cookie2022/commits/451ccfcb4f5b5d00fd97292e8d850ed86f04ef84))



# [2.1.0](https://bitbucket.org/superkoders/cookie2022/compare/v2.0.4...v2.1.0) (2022-01-28)


### Bug Fixes

* default popup width ([62bae86](https://bitbucket.org/superkoders/cookie2022/commits/62bae865be9da10b0829c81bd3d80376005d7f51))
* update webpack and babel transpilation ([fee1ac8](https://bitbucket.org/superkoders/cookie2022/commits/fee1ac806247c4561ac8a11acdb05af880cdeef2))


### Features

* add compiled version to npm ([36c8025](https://bitbucket.org/superkoders/cookie2022/commits/36c80253eaa7c6765a9b5f69c6e3223accd259e8))



## [2.0.4](https://bitbucket.org/superkoders/cookie2022/compare/v2.0.3...v2.0.4) (2022-01-17)


### Bug Fixes

* cookie name ([203f368](https://bitbucket.org/superkoders/cookie2022/commits/203f368a83164136490e472d9b8a6fce2a8c9751))



## [2.0.3](https://bitbucket.org/superkoders/cookie2022/compare/v2.0.2...v2.0.3) (2022-01-17)


### Bug Fixes

* send beacon ([7a20de7](https://bitbucket.org/superkoders/cookie2022/commits/7a20de7f1be6b8911712d29c0c0963f9efbcedf7))



## [2.0.2](https://bitbucket.org/superkoders/cookie2022/compare/v2.0.1...v2.0.2) (2022-01-17)


### Bug Fixes

* add z-index ([3e73bee](https://bitbucket.org/superkoders/cookie2022/commits/3e73bee7866ed103090f320f6579dbf14db8cbc9))
* demo ([0e7751d](https://bitbucket.org/superkoders/cookie2022/commits/0e7751d315e8343234335604c7dd11a00d309492))
* not clickable labels ([c066543](https://bitbucket.org/superkoders/cookie2022/commits/c0665438c54a94f9a78610bb23bd73e92ee177de))



## [2.0.1](https://bitbucket.org/superkoders/cookie2022/compare/v2.0.0...v2.0.1) (2022-01-14)


### Bug Fixes

* remove engine check ([e0773c2](https://bitbucket.org/superkoders/cookie2022/commits/e0773c2c110e3128c1a3948809837e6958636988))



# [2.0.0](https://bitbucket.org/superkoders/cookie2022/compare/61e987edb68513be52d1a4ebb13d1149cea18e7b...v2.0.0) (2022-01-14)


### Bug Fixes

* add functional storage to header ([61e987e](https://bitbucket.org/superkoders/cookie2022/commits/61e987edb68513be52d1a4ebb13d1149cea18e7b))
* default texts and their order ([8eb0bd3](https://bitbucket.org/superkoders/cookie2022/commits/8eb0bd338bcf5fdcd56b6eadf85b3ea35d6cb0b7))
* main desc ([c05756e](https://bitbucket.org/superkoders/cookie2022/commits/c05756e1273e52a365a8a69f2ffcd4e9e83cee88))
* marketing title ([62bacea](https://bitbucket.org/superkoders/cookie2022/commits/62baceaefe7f16edd6f8720c3b40a9190ca57d86))
* readme ([68ee106](https://bitbucket.org/superkoders/cookie2022/commits/68ee106228f3b637594ca23aa9f79b9abe032fc4))
* stylelint ([7a9c39c](https://bitbucket.org/superkoders/cookie2022/commits/7a9c39cbbfea41979a11eadb73d9349b1c1dd402))



