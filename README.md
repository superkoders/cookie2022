# SUPERKODERS Cookie

Package for our custom "cookie" popup implementation.
Everything what you need to know is in our [Notion](https://www.notion.so/superkoders/Cookies-od-1-1-2022-b00661dc9cf4459892b9448d70d50ec4).

Package expose files (source and compiled)

1. base css for popup style 
2. vanilla javascript module for interaction management.

## Implementation
### Option 1: Add to build
```scss
// project css/components/box/cookie.scss

@import @superkoders/cookie/css/components/box/cookie

.b-cookie {
	&__box {
		background: #000;
	}
}
```
```javascript
// app.js

import * as cookie from '@superkoders/cookie/js/components/cookie.js';

cookie.init();
```

### Option 2: Use compiled version
link css
```html
<link rel="stylesheet" href="https://unpkg.com/@superkoders/cookie@latest/dist/css/cookie.css">
```
link js
```html
<script src="https://unpkg.com/@superkoders/cookie@latest/dist/js/cookie.js"></script>
```

## Javascript API
exposed method from javascript module
```javascript
// start cookie popup interactivity
init()

// open cookie popup again
open()

// subscribe to cookie event - update, delete
on(type, fn); 
// subscribe to cookie event only once
one(type, fn);
// unsubscribe to cookie event
off(type, fn);

// current cookie object
getCurrentState(); 
// delete all user votes
deleteConsent();
// check if user voted
didUserConsent(); 

// check if user accepted some category
// ['functional_storage', 'ad_storage', 'analytics_storage', 'personalization_storage']
allowedCategory('functional_storage'); 

// run lazyevaluated embeds
heckEmbeds();
// run lazyevaluated scripts
checkScripts();
```
When compiled package is linked all API is exported to window.SKcookieAPI.
```javascript
window.SKcookieAPI.init();
window.SKcookieAPI.checkScripts();
window.SKcookieAPI.on('update', window.SKcookieAPI.checkScripts);
```

## Author

* **SUPERKODERS** - [www.superkoders.com](https://superkoders.com/) – [support@superkoders.com](support@superkoders.com)
